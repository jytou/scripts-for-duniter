# scripts-for-duniter

Some scripts that help me run duniter on the command line with minimum hassle.

The flow goes like this:

./resync.sh ; ./watchit.sh

If your node crashes, the watchit script will restart it automatically.

When your node is not in sync for any reason, just [Ctrl]-C and relaunch the command.

If you want to restart the computer (for an update, for instance), then just [Ctrl]-C and duniter stop. When the computer restarts, just call ./watchit.sh

Bien sûr, le script resync.sh peut être personnalisé pour pointer vers le nœud voulu (typiquement un autre nœud pour gtest !).