#!/bin/bash
while true
do
	if [ "`ps -e|grep duniter|grep -v grep`" == "" ]
	then
		echo "Restarting Duniter at `date`..."
		duniter start
	else
		sleep 1m
	fi
done

